using System.Collections;
using UnityEngine;
using Exploder.Utils;

public class Bullet : MonoBehaviour
{
    public BulletType bulletType;
    private float ATK;
    public Rigidbody rigidbody;

    private void OnEnable()
    {
        StartCoroutine(Hide());
    }

    private void OnDisable()
    {
        StopCoroutine(Hide());
        rigidbody.isKinematic = true;
    }

    private IEnumerator Hide()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }

    public void SetATK(float ATK) => this.ATK = ATK;

    private void OnCollisionEnter(Collision collision)
    {
        BulletEntity bulletEntity = DataManager.Instance.BulletsDB.bullets.Find(b => b.bulletType == bulletType);

        if (collision.gameObject.CompareTag(TagDefine.Exploder.ToString()))
        {
            ExploderSingleton.Instance.ExplodeObject(collision.gameObject);
            SoundManager.Instance.PlayVFXSound(bulletEntity.hitSound);
        }    

        if (collision.gameObject.CompareTag(TagDefine.Player.ToString())) {
            collision.collider.GetComponent<Player>().TakeDame(ATK);
            Debug.Log(2222);
        }

        if (collision.gameObject.CompareTag(TagDefine.Bot.ToString()))
        {
            collision.collider.GetComponent<Bot>().TakeDame(ATK);
            Debug.Log(3333);
        }

        Instantiate(bulletEntity.hitEffect, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
    }
}


