using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoseUI : View
{
    [SerializeField] private Button btnHome;

    // Start is called before the first frame update
    void Start()
    {
        btnHome.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(0);
        });
    }
}
