using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TankSelectionUI : View
{
    [Header("BUTTON:")]
    [SerializeField] private Button btnCloseUI;
    [SerializeField] private Button btnNextTankSkin;
    [SerializeField] private Button btnPreviousTankSkin;
    [SerializeField] private Button btnEasyLevel;
    [SerializeField] private Button btnNormalLevel;
    [SerializeField] private Button btnHardLevel;
    [SerializeField] private Button btnReadyBattle;

    [Space]
    [Header("TXT:")]
    [SerializeField] private TextMeshProUGUI txtTankName;

    private int skinIdx = 0;
    private Transform skinInstance;

    private void Start()
    {       
        ChangeSkin();

        ChangeColorLevelButton(btnEasyLevel,DifficultyLevels.easy);

        btnReadyBattle.onClick.AddListener(() =>
        {
            Destroy(skinInstance.gameObject);
            UIManager.Instance.OnShow(UIManager.Instance.loadingUI_Prefab);
        });

        btnCloseUI.onClick.AddListener(() =>
        {
            Destroy(skinInstance.gameObject);
            UIManager.Instance.OnShow(UIManager.Instance.homeUI_Prefab);
        });

        btnNextTankSkin.onClick.AddListener(() =>
        {
            NextSkin();
        });

        btnPreviousTankSkin.onClick.AddListener(() =>
        {
            PreviousSkin();
        });

        btnEasyLevel.onClick.AddListener(() =>
        {
            ChangeColorLevelButton(btnEasyLevel, DifficultyLevels.easy);
        });

        btnNormalLevel.onClick.AddListener(() =>
        {
            ChangeColorLevelButton(btnNormalLevel, DifficultyLevels.normal);
        });

        btnHardLevel.onClick.AddListener(() =>
        {
            ChangeColorLevelButton(btnHardLevel, DifficultyLevels.hard);
        });
    }

    private void NextSkin()
    {
        skinIdx++;
        if (skinIdx == DataManager.Instance.TankDB.tanks.Count)
        {
            skinIdx = 0;
        }
        ChangeSkin();
    }

    private void PreviousSkin()
    {
        skinIdx--;
        if (skinIdx < 0)
        {
            skinIdx = DataManager.Instance.TankDB.tanks.Count - 1;
        }
        ChangeSkin();
    }

    private void ChangeSkin()
    {
        GameManager.Instance.skinParent.rotation = Quaternion.identity;
        if (skinInstance != null)
        {
            Destroy(skinInstance.gameObject);
        }
        var tankData = DataManager.Instance.TankDB.tanks[skinIdx];
        txtTankName.text = tankData.name;
        skinInstance = Instantiate(tankData.model,GameManager.Instance.skinParent);
        skinInstance.SetParent(GameManager.Instance.skinParent);
    }

    private void ChangeColorLevelButton(Button btnSelected, DifficultyLevels difficultyLevel)
    {
        btnEasyLevel.image.color = Color.white;
        btnNormalLevel.image.color = Color.white;
        btnHardLevel.image.color = Color.white;

        btnSelected.image.color = Color.green;
        GameManager.Instance.difficultyLevel = difficultyLevel;
    }
}
