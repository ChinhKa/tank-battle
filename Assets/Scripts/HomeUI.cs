using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeUI : View
{
    [SerializeField] private Button btnPlay;

    private void Start()
    {
        btnPlay.onClick.AddListener(() =>
        {
            UIManager.Instance.OnShow(UIManager.Instance.tankSelectionUI_Prefab);
        });
    }
}
