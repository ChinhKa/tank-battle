public class ContantKey
{
     
}

public enum GameStatus
{ 
    start,
    ready,
    finish,
    pause
}

public enum BulletType
{
    rocket
}

public enum TagDefine
{
    Exploder,
    Player,
    Bot
}

public enum TankID
{
    T54,
    T90
}

public enum GameModes
{
    hunt,
    defense,
    siege
}

public enum DifficultyLevels
{
    easy,
    normal,
    hard
}

public enum ActionDefine
{

}