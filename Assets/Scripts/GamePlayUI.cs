using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GamePlayUI : View
{
    [Header("COOLDOWN:")]
    [SerializeField] private Slider coolDownSlider;
    [SerializeField] private Slider healthSlider;
    [SerializeField] private TextMeshProUGUI txtCoolDown_Shoot;
    [SerializeField] private TextMeshProUGUI txtHealthSlider;
    [SerializeField] private TextMeshProUGUI txtBulletsCount;
    [SerializeField] private TextMeshProUGUI txtCoolDown_BattleTime;
    [SerializeField] private TextMeshProUGUI txtVelocity;

    [Space]
    [Header("SCOPE:")]
    public Transform scopeFrame;

    public FixedJoystick joystick;

    private void Start()
    {
        scopeFrame.GetChild(0).gameObject.SetActive(false);
    } 

    #region HealthBar
    public void SetMaxHealthSliderValue(float maxValue)
    {
        healthSlider.maxValue = maxValue;
        healthSlider.value = maxValue;
        txtHealthSlider.text = maxValue.ToString();
    }

    public void UpdateHealthSlider(float value)
    {
        healthSlider.value -= value;
        txtHealthSlider.text = healthSlider.value <= 0 ? "0" : healthSlider.value.ToString();
    }
    #endregion


    #region CoolDown
    public void SetMaxCoolDown_ShootSliderValue(float maxValue)
    {
        coolDownSlider.maxValue = maxValue;
        coolDownSlider.value = 0;
    }

    public void UpdateCoolDown_ShootSlider(float value)
    {
        coolDownSlider.value = value;
        txtCoolDown_Shoot.text = coolDownSlider.value <= 0 ? "Shoot" : string.Format("{0:0.0}", value);
    }
    #endregion

    public void SetTxtBulletCount(int value) => txtBulletsCount.text = value.ToString();

    public void SetTxtVelocity(string value) => txtVelocity.text = value + " km/h";

    public void SetTxtBattleTime(float value) => txtCoolDown_BattleTime.text = Mathf.Floor(value / 60).ToString("00") + ":" + (value % 60).ToString("00");
}
