using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoSingleton<DataManager>
{
    public BulletsDB BulletsDB;
    public TankDB TankDB;
    [Space]
    [Header("MODE:")]
    public HuntModeDB HuntModeDB;
}
