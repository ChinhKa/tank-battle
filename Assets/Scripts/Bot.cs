﻿using ProjectDawn.LocalAvoidance.Demo;
using UnityEngine;

public class Bot : TankBase
{
    private Transform target;

    public override void Start()
    {
        base.Start();
        target = GameObject.FindGameObjectWithTag(TagDefine.Player.ToString()).transform;
    }

    public override void Rotate()
    {
        Vector3 direction = target.position - artilleryPedestal.position;
        direction.y = 0;
        Quaternion rotation = Quaternion.LookRotation(direction);
        artilleryPedestal.rotation = Quaternion.Slerp(artilleryPedestal.rotation, rotation, tankData.rotationSpeed_artilleryPedestal * Time.deltaTime);
    }

    public override void Shoot()
    {
        if (EnableShoot())
            HandlingShoot();
    }

    public override void SetMaxHealthSlider(float maxValue)
    {
        healthSlider.maxValue = maxValue;
        healthSlider.value = maxValue;
    }

    public override void TakeDame(float atk)
    {
        healthSlider.value -= atk;
        if(healthSlider.value <= 0)
        {
            GetComponent<BoidsAgent>().stop = true;
            GetComponent<BoidsAgent>().Stop();
            Die();
        }
    }
}
