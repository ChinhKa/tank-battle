using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinRotation : MonoBehaviour
{
    private void OnMouseDrag()
    {
        transform.Rotate(new Vector3(0, Input.GetAxisRaw("Mouse X") * 360 * Time.deltaTime, 0));
    }
}
