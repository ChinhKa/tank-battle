﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Player : TankBase
{
    [SerializeField] private FixedJoystick joystick;
    [SerializeField] protected Rigidbody rigidbody;
    protected bool zoomedIn;
    protected float xRotation_Cannon;

    public override void Start()
    {
        base.Start();

        SetJoystick();
        SetMaxHealthSlider(tankData.HP);
        SetMaxBulletCount();
    }

    public override void Update()
    {
        base.Update();

        if (GameManager.Instance.gameStatus == GameStatus.ready)
        {
            UpdateCoolDown_ShootSlider();

            if (Input.GetKeyDown(KeyCode.Z)) 
            {
                Scope();
            }
        }
    }

    private void SetJoystick()
    {
        GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
        joystick = gamePlayUI.joystick;
    }

    public void Move()
    {
        float speed = tankData.moveSpeed;

        if (Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift))
        {
            speed *= 2;
        }

        rigidbody.velocity = new Vector3(joystick.Horizontal * speed, rigidbody.velocity.y, joystick.Vertical * speed);
        if (joystick.Horizontal != 0 || joystick.Vertical != 0)
        {
            transform.rotation = Quaternion.LookRotation(rigidbody.velocity);

            if (!audioSource.isPlaying)
                audioSource.PlayOneShot(tankData.runSound);
        }
        else
        {
            if (audioSource.isPlaying)
                audioSource.Stop();
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.Instance.gameStatus == GameStatus.ready)
        {
            #region move quick
            float speedInMetersPerSecond = rigidbody.velocity.magnitude;
            float speedInKph = speedInMetersPerSecond * 3.6f;

            string formattedSpeed = speedInKph.ToString("F1");

            GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
            gamePlayUI.SetTxtVelocity(formattedSpeed);
            #endregion

            Move();
        }
    }

    public override void Rotate()
    {
        RotateCannon();
        RotateTank();
        RotateArtilleryPedestal();
    }

    public virtual void RotateArtilleryPedestal()
    {
        if (Input.GetKey(KeyCode.L))
        {
            artilleryPedestal.Rotate(Vector3.up, tankData.rotationSpeed_Tank * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.J))
        {
            artilleryPedestal.Rotate(-Vector3.up, tankData.rotationSpeed_Tank * Time.deltaTime);
        }
    }

    public virtual void RotateCannon()
    {
        if (Input.GetKey(KeyCode.I))
        {
            xRotation_Cannon += tankData.rotationSpeed_Cannon * Time.deltaTime;
            xRotation_Cannon = Mathf.Clamp(xRotation_Cannon, -20, 10);
            cannon.localRotation = Quaternion.Euler(xRotation_Cannon, 0, 0);
        }

        if (Input.GetKey(KeyCode.K))
        {
            xRotation_Cannon -= tankData.rotationSpeed_Cannon * Time.deltaTime;
            xRotation_Cannon = Mathf.Clamp(xRotation_Cannon, -20, 10);
            cannon.localRotation = Quaternion.Euler(xRotation_Cannon, 0, 0);
        }
    }

    public virtual void RotateTank()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(-Vector3.up, tankData.rotationSpeed_Tank * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(Vector3.up, tankData.rotationSpeed_Tank * Time.deltaTime);
        }
    }

    public virtual void Scope()
    {
        GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
        if (!zoomedIn)
        {
            gamePlayUI.scopeFrame.GetChild(0).gameObject.SetActive(true);
            Camera.main.DOFieldOfView(20, 0.2f);
            zoomedIn = true;
        }
        else
        {
            gamePlayUI.scopeFrame.GetChild(0).gameObject.SetActive(false);
            Camera.main.DOFieldOfView(60, 0.2f);
            zoomedIn = false;
        }
    }

    public override void Shoot()
    {
        try
        {
            if (Input.GetKeyDown(KeyCode.Space) && EnableShoot())
            {
                HandlingShoot();
                Camera.main.transform.DOShakePosition(0.5f, 0.2f);
            }
        }
        catch (System.Exception ex)
        {
            Debug.LogWarning("Warning: " + ex);
        }
    }

    public override void HandlingShoot()
    {
        base.HandlingShoot();
        bulletCount--;
        GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
        gamePlayUI.SetTxtBulletCount(bulletCount);
    }

    public virtual void UpdateCoolDown_ShootSlider()
    {
        GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
        gamePlayUI.UpdateCoolDown_ShootSlider(coolDown_Shoot);

        if (bulletCount <= 0 && coolDown_Shoot <= 0)
        {
            SetMaxBulletCount();
        }
    }

    public override void SetMaxHealthSlider(float maxValue)
    {
        GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
        gamePlayUI.SetMaxHealthSliderValue(maxValue);
    }

    public virtual void SetMaxBulletCount()
    {
        tankData = DataManager.Instance.TankDB.tanks.Find(t => t.tankID == tankID);
        bulletCount = tankData.bulletCount;
        GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
        gamePlayUI.SetTxtBulletCount(bulletCount);
    }

    public override void TakeDame(float atk)
    {
        GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
        gamePlayUI.UpdateHealthSlider(atk);
       /* if (gamePlayUI.coolDownSlider.value <= 0)
            Die();*/
    }
}
