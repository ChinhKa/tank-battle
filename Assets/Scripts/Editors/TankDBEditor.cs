using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
[CustomEditor(typeof(TankDB))]
public class TankDBEditor : Editor
{
    string path = "Assets/Scripts/TankDB.asset";

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Save"))
        {
            Save();
        }
    }

    public void Save()
    {
        EditorUtility.SetDirty(AssetDatabase.LoadAssetAtPath(path, typeof(TankDB)));
        AssetDatabase.SaveAssets();
    }
}
#endif
