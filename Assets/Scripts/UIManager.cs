using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField] private Transform contain;

    [HideInInspector] public View view;

    [Space]
    [Header("UI PREFABS:")]
    public Transform homeUI_Prefab;
    public Transform gamePlayUI_Prefab;
    public Transform winUI_Prefab;
    public Transform loseUI_Prefab;
    public Transform tankSelectionUI_Prefab;
    public Transform loadingUI_Prefab;

    private void Start()
    {
        OnShow(homeUI_Prefab);
    }

    public void OnShow(Transform ui)
    {
        if(view != null)
        {
            Destroy(view.gameObject);
        }

        Transform instance = Instantiate(ui,contain);
        instance.SetParent(contain);
        view = instance.GetComponent<View>();
    }
}
