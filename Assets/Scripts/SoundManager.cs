using UnityEngine;

public class SoundManager : MonoSingleton<SoundManager>
{
    [Header("AUDIO SOURCE:")]
    [SerializeField] private AudioSource BGSource;
    [SerializeField] public AudioSource VFXSource;

    [Space]
    [Header("AUDIO CLIP:")]
    public AudioClip bgSound;
    public AudioClip crackingSound;

    public void PlayBGSound(AudioClip clip)
    {
        if (BGSource != null && clip != null)
        {
            BGSource.clip = clip;
            BGSource.Play();
        }
    }

    public void PlayVFXSound(AudioClip clip)
    {
        if (VFXSource != null && clip != null)
            VFXSource.PlayOneShot(clip);
    }
}
