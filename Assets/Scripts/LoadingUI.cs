using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LoadingUI : View
{
    public TextMeshProUGUI txtPercent;
    public Slider sliderLoad;

    private void Start()
    {
        sliderLoad.maxValue = 100;
        sliderLoad.value = 0;
        txtPercent.text = "0%";
        LoadScene();
    }

    public void LoadScene()
    {
        StartCoroutine(LoadAsync());
    }

    IEnumerator LoadAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(1);
        while (!operation.isDone)
        {
            float progress = Mathf.Round(Mathf.Clamp01(operation.progress / .9f) * 100);
            txtPercent.text = progress + "%";
            sliderLoad.value = progress;
            if(sliderLoad.value == sliderLoad.maxValue)
            {
                GameManager.Instance.readyBattle?.Invoke();
            }
            yield return null;
        }
    }
}
