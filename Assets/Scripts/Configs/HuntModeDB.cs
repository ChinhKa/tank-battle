using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HuntModeDB" , menuName = "DB/GameMode/HuntModeDB")]

public class HuntModeDB : ScriptableObject
{
    public List<DifficultyLevelsEntity> difficultyLevels = new List<DifficultyLevelsEntity>();
}

[System.Serializable]
public class DifficultyLevelsEntity
{
    public DifficultyLevels difficultyLevel;
    public int enemyCount;
    public float battleTime;
    [Range(0,100)]
    public float percentDamageReduction;
    [Range(0,100)]
    public float percentBloodReduction;
}