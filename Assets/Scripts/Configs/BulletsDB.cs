﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletDB", menuName = "DB/BulletDB")]
public class BulletsDB : ScriptableObject
{
    public List<BulletEntity> bullets = new List<BulletEntity>();
}

[System.Serializable]
public class BulletEntity
{
    public BulletType bulletType;

    [Header("EFFECT")]
    public Transform hitEffect;

    [Space]
    [Header("SOUND:")]
    public AudioClip attackSound;
    public AudioClip hitSound;
}
