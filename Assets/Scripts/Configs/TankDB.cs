using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TankDB", menuName = "DB/TankDB")]

public class TankDB : ScriptableObject
{
    public List<TankEntity> tanks = new List<TankEntity>();
}

[System.Serializable]
public class TankEntity
{
    public TankID tankID;
    public string name;
    [Range(1, 5000)]
    public float HP;
    [Range(1, 1000)]
    public float ATK;
    [Range(1, 100)]
    public int bulletCount;
    [Range(1, 100)]
    public float moveSpeed;
    [Range(1, 100)]
    public float ATKS;
    [Range(1, 100)]
    public float rotationSpeed_Tank;
    [Range(1, 100)]
    public float rotationSpeed_Cannon;
    [Range(1, 100)]
    public float rotationSpeed_artilleryPedestal;
    [Range(1, 100)]
    public float maximumRotationAngle_Cannon;
    [Range(-100, 100)]
    public float minimumRotationAngle_Cannon;
    [Range(1, 1000)]
    public float shootForce;
    [Header("SOUND:")]
    public AudioClip runSound;
    [Header("MODEL:")]
    public Transform model;
}