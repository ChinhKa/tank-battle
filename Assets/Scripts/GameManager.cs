using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoSingleton<GameManager>
{
    [Header("GAME STATES:")]
    public GameStatus gameStatus;
    public DifficultyLevels difficultyLevel;
    public GameModes gameMode;

    [Space]
    [Header("ACTION:")]
    public Action readyBattle;

    [Header("TANKS:")]
    public List<TankBase> listTank_P = new List<TankBase>();
    public List<TankBase> listTank_E = new List<TankBase>();

    [Space]
    [Header("SKIN:")]
    public Transform skinParent;

    [Space]
    [Header("BATTLE:")]
    public float countDownBattle;

    private void Start()
    {
        Reset();
        readyBattle += ReadyBattle;
    }

    private void Update()
    {
        if (gameStatus == GameStatus.ready)
        {
            CountDownBattle();
        }
    }

    public void Reset()
    {
        gameStatus = GameStatus.pause;
    }

    public void ReadyBattle()
    {
        Cursor.lockState = CursorLockMode.Locked;
        UIManager.Instance.OnShow(UIManager.Instance.gamePlayUI_Prefab);
        SoundManager.Instance.PlayBGSound(SoundManager.Instance.bgSound);

        switch (gameMode)
        {
            case GameModes.hunt:
                foreach (var g in DataManager.Instance.HuntModeDB.difficultyLevels)
                {
                    if (difficultyLevel == g.difficultyLevel)
                    {
                        countDownBattle = g.battleTime;
                        break;
                    }
                }
                break;
            case GameModes.defense:
                break;
            case GameModes.siege:
                break;
            default:
                break;
        }
        gameStatus = GameStatus.ready;
    }

    public void CountDownBattle()
    {
        countDownBattle -= Time.deltaTime;

        if(countDownBattle <= 0)
        {
            gameStatus = GameStatus.finish;
       /*     GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
            gamePlayUI.SetTxtBattleTime(0);*/

        }
        else
        {
            GamePlayUI gamePlayUI = UIManager.Instance.view as GamePlayUI;
            gamePlayUI.SetTxtBattleTime(countDownBattle);
        }

    }
}
