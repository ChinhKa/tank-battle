﻿using Exploder.Utils;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class TankBase : MonoBehaviour
{
    [SerializeField] protected TankID tankID;
    protected float HP;
    protected int bulletCount;
    [SerializeField] protected Slider healthSlider;
    [SerializeField] protected List<Rigidbody> accessory = new List<Rigidbody>();

    [Header("BULLET:")]
    [SerializeField] protected BulletType bulletType;

    [Space]
    [Header("TANK ELEMENTS:")]
    [SerializeField] protected Transform cannon;
    [SerializeField] protected Transform artilleryPedestal;

    [Space]
    [Header("EFFECT:")]
    [SerializeField] protected ParticleSystem flashEffect;

    [Space]
    [Header("AUDIO SOURCE:")]
    [SerializeField] protected AudioSource audioSource;

    [Space]
    [Header("DATA:")]
    protected TankEntity tankData;

    protected float coolDown_Shoot;

    public virtual void Start()
    {
        LoadData();
        Accessory();
    }

    public virtual void Update()
    {
        if (GameManager.Instance.gameStatus == GameStatus.ready)
        {
            if (!EnableShoot())
            {
                coolDown_Shoot -= Time.deltaTime;
            }
            Rotate();
            Shoot();
        }
    }

    public TankID GetTankID() => tankID;

    public virtual bool EnableShoot()
    {
        if(coolDown_Shoot <= 0)
        {
            return true;
        }

        return false;
    }

    public abstract void Rotate();

    public abstract void Shoot();

    public abstract void TakeDame(float atk);

    public virtual void Die()
    {
        healthSlider.gameObject.SetActive(false);
        Explode();
        this.enabled = false;
    }

    public abstract void SetMaxHealthSlider(float maxValue);

    public void Explode()
    {
        foreach(var o in accessory)
        {
            o.isKinematic = false;
            o.AddForce(o.transform.up * 20,ForceMode.Impulse);
        }
    }

    public void Accessory()
    {
        foreach (var o in accessory)
        {
            o.isKinematic = true;
        }
    }

    public void LoadData()
    {
        tankData = DataManager.Instance.TankDB.tanks.Find(t => t.tankID == tankID);       
        SetMaxHealthSlider(tankData.HP);
    }

    public virtual void HandlingShoot()
    {
        AudioClip attackSound = DataManager.Instance.BulletsDB.bullets.Find(b => b.bulletType == bulletType).attackSound;
        SoundManager.Instance.PlayVFXSound(attackSound);
        flashEffect.Play();
        GameObject bullet = PoolManager.Instance.GetPool(bulletType);
        bullet.transform.position = flashEffect.transform.position;
        bullet.transform.LookAt(cannon);
        bullet.GetComponent<Bullet>().rigidbody.isKinematic = false;
        bullet.GetComponent<Bullet>().rigidbody.AddForce(cannon.forward * tankData.shootForce, ForceMode.Impulse);
        bullet.GetComponent<Bullet>().SetATK(tankData.ATK);
        ResetCoolDown();
    }

    public virtual void ResetCoolDown() => coolDown_Shoot = tankData.ATKS;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(TagDefine.Exploder.ToString()))
        {
            ExploderSingleton.Instance.ExplodeObject(collision.gameObject);
        }
    }
}
