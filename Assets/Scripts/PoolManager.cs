using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoSingleton<PoolManager>
{
    private Dictionary<BulletType, List<GameObject>> bulletPools = new Dictionary<BulletType, List<GameObject>>();

    public List<GameObject> bulletPrefabs = new List<GameObject>();

    [Range(1, 30)]
    public int amountPool;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        foreach (var p in bulletPrefabs)
        {
            CreatePool(p);
        }
    }

    public void CreatePool(GameObject bulletPrefab)
    {
        List<GameObject> bulletPool = new List<GameObject>();
        for (int i = 0; i < amountPool; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.SetActive(false);
            bulletPool.Add(bullet);
        }
        bulletPools[bulletPrefab.GetComponent<Bullet>().bulletType] = bulletPool;
    }

    public GameObject GetPool(BulletType bulletType)
    {
        foreach (GameObject bullet in bulletPools[bulletType])
        {
            if (!bullet.activeInHierarchy)
            {
                bullet.SetActive(true);
                return bullet;
            }
        }

        GameObject bulletFind = bulletPrefabs.Find(p => p.GetComponent<Bullet>().bulletType == bulletType);
        GameObject bulletExpand = Instantiate(bulletFind);
        bulletPools[bulletType].Add(bulletExpand);
        return bulletExpand;
    }
}

